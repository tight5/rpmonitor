package main

import (
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/adamryman/kit/logger"
	"github.com/datadog/datadog-go/statsd"

	htu21d "github.com/idahoakl/HTU21D-sensor/src"
	ii2c "github.com/idahoakl/go-i2c"

	/*
		"github.com/d2r2/go-bsbmp"
		di2c "github.com/d2r2/go-i2c"
	*/

	"gitlab.com/tight5/go-am2315"
)

func main() {
	sigs := make(chan os.Signal, 1)
	done := make(chan bool, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	c, err := statsd.New("127.0.0.1:8125")
	if err != nil {
		logger.LogError(err)
	}
	c.Namespace = "tight5.service."

	ami2c, err := am2315.InitI2C()
	if err != nil {
		logger.LogError(err)
		os.Exit(1)
	}
	defer ami2c.Close()

	htui2c, err := ii2c.NewI2C(1)
	if err != nil {
		logger.LogError(err)
		os.Exit(1)
	}
	defer htui2c.Close()

	htu, err := htu21d.New(0x40, htui2c)
	if err != nil {
		logger.LogError(err)
		os.Exit(1)
	}

	/*
		bmpi2c, err := di2c.NewI2C(0x77, 1)
		if err != nil {
			log.Fatal(err)
		}
		defer bmpi2c.Close()

		bmp280, err := bsbmp.NewBMP(bsbmp.BMP280, bmpi2c)
		if err != nil {
			log.Fatal(err)
		}
	*/

	go func() {
		<-sigs
		done <- true
	}()

	logger.Info().Log("msg", "reading metrics")
FOR:
	for {
		select {
		case <-done:
			break FOR
		case <-time.Tick(time.Second * 5):
		}

		t, h, err := am2315.Read()
		if err != nil {
			logger.LogError(err)
			continue
		}
		logger.Info().Log("sensor", "am2315", "temp", t, "humidity", h)
		c.Gauge("sensor.temperature", t, []string{"sensor:am2315"}, 1)
		c.Gauge("sensor.humidity", h, []string{"sensor:am2315"}, 1)
		t2, err := htu.ReadTemperatureC()
		if err != nil {
			logger.LogError(err)
			continue
		}
		h2, err := htu.ReadHumidity()
		if err != nil {
			logger.LogError(err)
			continue
		}
		logger.Info().Log("sensor", "htu21d", "temp", t2, "humidity", h2)
		c.Gauge("sensor.temperature", float64(t2), []string{"sensor:htu21d"}, 1)
		c.Gauge("sensor.humidity", float64(h2), []string{"sensor:htu21d"}, 1)
	}
}
