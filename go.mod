module gitlab.com/tight5/rpmonitor

require (
	github.com/Sirupsen/logrus v1.3.0 // indirect
	github.com/VividCortex/gohistogram v1.0.0 // indirect
	github.com/adamryman/kit v0.0.0-20171013184434-80a6fafe29c0
	github.com/d2r2/go-bsbmp v0.0.0-20190224164347-9b5539f523d0
	github.com/d2r2/go-i2c v0.0.0-20181113114621-14f8dd4e89ce
	github.com/datadog/datadog-go v0.0.0-20180822151419-281ae9f2d895
	github.com/go-kit/kit v0.8.0
	github.com/go-logfmt/logfmt v0.4.0 // indirect
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/idahoakl/HTU21D-sensor v0.0.0-20171030161701-fe40d79cc567
	github.com/idahoakl/go-i2c v0.0.0-20171110060145-1be2f9864e4e
	github.com/op/go-logging v0.0.0-20160315200505-970db520ece7 // indirect
	github.com/sigurn/crc8 v0.0.0-20160107002456-e55481d6f45c // indirect
	github.com/sigurn/utils v0.0.0-20151230205143-f19e41f79f8f // indirect
	github.com/stianeikeland/go-rpio/v4 v4.4.0
	gitlab.com/tight5/go-am2315 v0.0.0
)

replace gitlab.com/tight5/go-am2315 => /home/zaq/projects/go-mod/src/gitlab.com/tight5/go-am2315

replace github.com/Sirupsen/logrus => github.com/sirupsen/logrus v1.3.0
